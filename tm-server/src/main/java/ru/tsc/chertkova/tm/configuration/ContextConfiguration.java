package ru.tsc.chertkova.tm.configuration;

import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.hibernate.event.service.spi.EventListenerRegistry;
import org.hibernate.event.spi.EventType;
import org.hibernate.internal.SessionFactoryImpl;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Scope;
import ru.tsc.chertkova.tm.api.service.IPropertyService;
import ru.tsc.chertkova.tm.listener.EntityListener;
import ru.tsc.chertkova.tm.model.Project;
import ru.tsc.chertkova.tm.model.Task;
import ru.tsc.chertkova.tm.model.User;
import ru.tsc.chertkova.tm.model.dto.ProjectDTO;
import ru.tsc.chertkova.tm.model.dto.TaskDTO;
import ru.tsc.chertkova.tm.model.dto.UserDTO;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;

@ComponentScan("ru.tsc.chertkova.tm")
public class ContextConfiguration {

    @Bean
    @NotNull
    public EntityManagerFactory entityManagerFactory(
            @NotNull final IPropertyService propertyService,
            @NotNull final EntityListener entityListener
    ) {
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(Environment.DRIVER, propertyService.getDatabaseDriver());
        settings.put(Environment.URL, propertyService.getDatabaseUrl());
        settings.put(Environment.USER, propertyService.getDatabaseUsername());
        settings.put(Environment.PASS, propertyService.getDatabasePassword());
        settings.put(Environment.DIALECT, propertyService.getDatabaseDialect());
        settings.put(Environment.HBM2DDL_AUTO, propertyService.getDatabaseHBM2DDL());
        settings.put(Environment.SHOW_SQL, propertyService.getDatabaseShowSQL());
        settings.put(Environment.USE_SECOND_LEVEL_CACHE, propertyService.getDatabaseUseSecondLvlCache());
        settings.put(Environment.USE_QUERY_CACHE, propertyService.getDatabaseUseQueryCache());
        settings.put(Environment.USE_MINIMAL_PUTS, propertyService.getDatabaseUseMinimalPuts());
        settings.put(Environment.CACHE_REGION_PREFIX, propertyService.getDatabaseCacheRegionPrefix());
        settings.put(Environment.CACHE_REGION_FACTORY, propertyService.getDatabaseCacheFactoryClass());
        settings.put(Environment.CACHE_PROVIDER_CONFIG, propertyService.getDatabaseCacheConfigFile());

        @NotNull final StandardServiceRegistryBuilder registryBuilder =
                new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources sources = new MetadataSources(registry);
        sources.addAnnotatedClass(UserDTO.class);
        sources.addAnnotatedClass(ProjectDTO.class);
        sources.addAnnotatedClass(TaskDTO.class);
        sources.addAnnotatedClass(User.class);
        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(Task.class);
        @NotNull final Metadata metadata = sources.getMetadataBuilder().build();
        EntityManagerFactory factory = metadata.getSessionFactoryBuilder().build();
        initListeners(factory, entityListener);
        return factory;
    }

    @Bean
    @NotNull
    @Scope("prototype")
    public EntityManager getEntityManager(@NotNull final EntityManagerFactory entityManagerFactory) {
        return entityManagerFactory.createEntityManager();
    }

    private void initListeners(@NotNull final EntityManagerFactory factory,
                               @NotNull final EntityListener entityListener) {
        @NotNull final SessionFactoryImpl sessionFactory = factory.unwrap(SessionFactoryImpl.class);
        @NotNull final EventListenerRegistry registry = sessionFactory
                .getServiceRegistry().getService(EventListenerRegistry.class);
        registry.getEventListenerGroup(EventType.POST_INSERT).appendListener(entityListener);
        registry.getEventListenerGroup(EventType.POST_DELETE).appendListener(entityListener);
        registry.getEventListenerGroup(EventType.POST_UPDATE).appendListener(entityListener);
        registry.getEventListenerGroup(EventType.PRE_INSERT).appendListener(entityListener);
        registry.getEventListenerGroup(EventType.PRE_DELETE).appendListener(entityListener);
        registry.getEventListenerGroup(EventType.PRE_UPDATE).appendListener(entityListener);
        registry.getEventListenerGroup(EventType.POST_LOAD).appendListener(entityListener);
    }

}
