package ru.tsc.chertkova.tm.repository.model;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.chertkova.tm.api.repository.model.IAbstractUserOwnerModelRepository;
import ru.tsc.chertkova.tm.model.AbstractFieldsModel;

import java.util.List;

@Repository
@Scope("prototype")
@AllArgsConstructor
public abstract class AbstractUserOwnerModelRepository<M extends AbstractFieldsModel>
        extends AbstractRepository<M> implements IAbstractUserOwnerModelRepository<M> {

    @Override
    public void add(@NotNull M model) {
        super.add(model);
    }

    @Override
    public abstract void clear();

    @Override
    @NotNull
    public abstract List<M> findAll();

    @Override
    @Nullable
    public abstract M findById(@NotNull String id);

    @Override
    public abstract int getSize();

    @Override
    public abstract M removeById(@NotNull String id);

    @Override
    public abstract void update(@NotNull M model);

    @Override
    public abstract void add(@NotNull String userId, @NotNull M model);

    @Override
    public abstract void clear(@NotNull String userId);

    @Override
    @NotNull
    public abstract List<M> findAll(@NotNull String userId);

    @Override
    @Nullable
    public abstract M findById(@NotNull String userId, @NotNull String id);

    @Override
    public abstract int getSize(@NotNull String userId);

    @Override
    public abstract M removeById(@NotNull String userId, @NotNull String id);

    @Override
    public abstract void update(@NotNull String userId, @NotNull M model);

}
