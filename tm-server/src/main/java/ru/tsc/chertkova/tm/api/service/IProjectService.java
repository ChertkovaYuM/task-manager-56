package ru.tsc.chertkova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.enumerated.Status;
import ru.tsc.chertkova.tm.model.Project;
import ru.tsc.chertkova.tm.model.Task;

import java.util.List;

public interface IProjectService {

    @Nullable
    Project add(@Nullable Project project);

    @Nullable
    Project updateById(
            @Nullable String id,
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description);

    @Nullable
    Project changeProjectStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status);

    boolean existsById(@Nullable String id);

    @Nullable
    Project findById(@Nullable String userId,
                     @Nullable String id);

    Project removeById(@Nullable String userId,
                       @Nullable String id);

    Project remove(@Nullable String userId,
                   @Nullable Project project);

    int getSize(@Nullable String userId);

    void clear(@Nullable String userId);

    @Nullable
    List<Project> findAll(@Nullable String userId);

    @Nullable
    List<Project> addAll(@NotNull List<Project> projects);

    @Nullable
    List<Project> removeAll(@Nullable List<Project> projects);

    @Nullable
    Task bindTaskToProject(@Nullable String userId,
                           @Nullable String projectId,
                           @Nullable String taskId);

}
