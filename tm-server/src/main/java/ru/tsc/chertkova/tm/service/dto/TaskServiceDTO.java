package ru.tsc.chertkova.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.tsc.chertkova.tm.api.repository.model.ITaskRepository;
import ru.tsc.chertkova.tm.api.service.ITaskService;
import ru.tsc.chertkova.tm.enumerated.Status;
import ru.tsc.chertkova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.chertkova.tm.exception.entity.StatusNotFoundException;
import ru.tsc.chertkova.tm.exception.entity.TaskNotFoundException;
import ru.tsc.chertkova.tm.exception.entity.UserNotFoundException;
import ru.tsc.chertkova.tm.exception.field.DescriptionEmptyException;
import ru.tsc.chertkova.tm.exception.field.IdEmptyException;
import ru.tsc.chertkova.tm.exception.field.NameEmptyException;
import ru.tsc.chertkova.tm.model.Task;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class TaskServiceDTO extends AbstractUserOwnerServiceDTO<Task> implements ITaskService {

    @NotNull
    public ITaskRepository getRepository() {
        return context.getBean(ITaskRepository.class);
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Task> findAllByProjectId(@Nullable final String userId,
                                         @Nullable final String projectId) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(projectId).orElseThrow(IdEmptyException::new);
        @NotNull final ITaskRepository taskRepository = getRepository();
        @NotNull final EntityManager entityManager = taskRepository.getEntityManager();
        @Nullable List<Task> tasks;
        try {
            entityManager.getTransaction().begin();
            tasks = taskRepository.findAllByProjectId(userId, projectId);
        } finally {
            entityManager.close();
        }
        return tasks;
    }

    @Override
    @Nullable
    public Task add(@Nullable final Task task) {
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        Optional.ofNullable(task.getUser().getId()).orElseThrow(UserNotFoundException::new);
        @NotNull final ITaskRepository taskRepository = getRepository();
        @NotNull final EntityManager entityManager = taskRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            Optional.ofNullable(task.getName()).orElseThrow(NameEmptyException::new);
            Optional.ofNullable(task.getProjectId()).orElseThrow(TaskNotFoundException::new);
            Optional.ofNullable(task.getUser().getId()).orElseThrow(TaskNotFoundException::new);
            taskRepository.add(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task updateById(@Nullable final String userId,
                           @Nullable final String id,
                           @Nullable final String name,
                           @Nullable final String description) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(description).orElseThrow(DescriptionEmptyException::new);
        @NotNull final ITaskRepository taskRepository = getRepository();
        @NotNull final EntityManager entityManager = taskRepository.getEntityManager();
        @Nullable Task task;
        try {
            entityManager.getTransaction().begin();
            task = findById(userId, id);
            Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
            taskRepository.update(task);
            entityManager.getTransaction().commit();
            task = findById(userId, id);
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task changeTaskStatusById(@Nullable final String userId,
                                     @Nullable final String id,
                                     @Nullable final Status status) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(status).orElseThrow(StatusNotFoundException::new);
        @NotNull final ITaskRepository taskRepository = getRepository();
        @NotNull final EntityManager entityManager = taskRepository.getEntityManager();
        @Nullable Task task;
        try {
            entityManager.getTransaction().begin();
            task = findById(userId, id);
            Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
            taskRepository.changeStatus(id, userId, status.getDisplayName());
            entityManager.getTransaction().commit();
            task = findById(userId, id);
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final ITaskRepository taskRepository = getRepository();
        @NotNull int count = taskRepository.existsById(id);
        return count > 0;
    }

    @Override
    public @Nullable Task findById(@Nullable final String userId,
                                   @Nullable final String id) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final ITaskRepository taskRepository = getRepository();
        @NotNull final EntityManager entityManager = taskRepository.getEntityManager();
        @Nullable Task task;
        try {
            entityManager.getTransaction().begin();
            task = taskRepository.findById(userId, id);
        } finally {
            entityManager.close();
        }
        return task;
    }

    @Override
    public Task removeById(@Nullable final String userId,
                           @Nullable final String id) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final ITaskRepository taskRepository = getRepository();
        @NotNull final EntityManager entityManager = taskRepository.getEntityManager();
        @NotNull final Task task;
        try {
            entityManager.getTransaction().begin();
            task = findById(userId, id);
            taskRepository.removeById(userId, id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

    @Override
    public Task remove(@Nullable final String userId,
                       @Nullable final Task task) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        Optional.ofNullable(findById(task.getUser().getId(), task.getId())).orElseThrow(TaskNotFoundException::new);
        removeById(task.getUser().getId(), task.getId());
        return task;
    }

    @Override
    public int getSize(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        @NotNull final ITaskRepository taskRepository = getRepository();
        @NotNull final EntityManager entityManager = taskRepository.getEntityManager();
        int size = 0;
        try {
            entityManager.getTransaction().begin();
            size = taskRepository.getSize(userId);
        } finally {
            entityManager.close();
        }
        return size;
    }

    @Override
    public void clear(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        @NotNull final ITaskRepository taskRepository = getRepository();
        @NotNull final EntityManager entityManager = taskRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            taskRepository.clear(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @Nullable
    public List<Task> findAll(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        @NotNull final ITaskRepository taskRepository = getRepository();
        @NotNull final EntityManager entityManager = taskRepository.getEntityManager();
        @Nullable List<Task> tasks;
        try {
            entityManager.getTransaction().begin();
            tasks = taskRepository.findAll(userId);
        } finally {
            entityManager.close();
        }
        return tasks;
    }

    @Override
    @Nullable
    public List<Task> addAll(@NotNull final List<Task> tasks) {
        Optional.ofNullable(tasks).orElseThrow(ProjectNotFoundException::new);
        @NotNull final ITaskRepository taskRepository = getRepository();
        @NotNull final EntityManager entityManager = taskRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            for (Task t : tasks) {
                taskRepository.add(t);
            }
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return tasks;
    }

    @Override
    @Nullable
    public List<Task> removeAll(@Nullable final List<Task> tasks) {
        Optional.ofNullable(tasks).orElseThrow(ProjectNotFoundException::new);
        @NotNull final ITaskRepository taskRepository = getRepository();
        @NotNull final EntityManager entityManager = taskRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            for (Task t : tasks) {
                taskRepository.removeById(t.getUser().getId(), t.getId());
            }
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return tasks;
    }

}
