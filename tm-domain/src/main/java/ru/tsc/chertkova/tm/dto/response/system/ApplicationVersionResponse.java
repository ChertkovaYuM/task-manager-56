package ru.tsc.chertkova.tm.dto.response.system;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.dto.response.AbstractResponse;

@Getter
@Setter
public final class ApplicationVersionResponse extends AbstractResponse {

    @Nullable
    private String version;

}
