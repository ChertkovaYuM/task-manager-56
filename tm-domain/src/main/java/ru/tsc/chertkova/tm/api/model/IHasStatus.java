package ru.tsc.chertkova.tm.api.model;

import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.enumerated.Status;

public interface IHasStatus {

    @Nullable
    Status getStatus();

    void setStatus(@Nullable Status status);

}
