package ru.tsc.chertkova.tm.listener;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import ru.tsc.chertkova.tm.api.service.ILoggerService;
import ru.tsc.chertkova.tm.model.EntityLog;
import ru.tsc.chertkova.tm.service.LoggerService;

import javax.jms.*;
import java.io.Serializable;

public class LoggerListener implements MessageListener {

    @NotNull
    @Autowired
    private final ILoggerService loggerService = new LoggerService();

    @Override
    @SneakyThrows
    public void onMessage(@NotNull final Message message) {
        if (!(message instanceof ObjectMessage)) return;
        @NotNull final Serializable entity = ((ObjectMessage) message).getObject();
        if (entity instanceof EntityLog) loggerService.writeLog((EntityLog) entity);
    }

}
