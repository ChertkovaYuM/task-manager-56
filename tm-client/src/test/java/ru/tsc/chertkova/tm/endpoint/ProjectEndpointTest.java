package ru.tsc.chertkova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.chertkova.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.chertkova.tm.api.endpoint.IProjectEndpoint;
import ru.tsc.chertkova.tm.api.service.IPropertyService;
import ru.tsc.chertkova.tm.dto.request.project.*;
import ru.tsc.chertkova.tm.dto.request.user.UserLoginRequest;
import ru.tsc.chertkova.tm.dto.response.project.*;
import ru.tsc.chertkova.tm.dto.response.user.UserLoginResponse;
import ru.tsc.chertkova.tm.enumerated.Status;
import ru.tsc.chertkova.tm.marker.IntegrationCategory;
import ru.tsc.chertkova.tm.model.Project;
import ru.tsc.chertkova.tm.service.PropertyService;

@Category(IntegrationCategory.class)
public class ProjectEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final String host = propertyService.getHost();

    @NotNull
    private final String port = propertyService.getPort();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(host, port);

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(host, port);

    @Nullable
    private String token;

    @Nullable
    private Project projectBefore;

    @Before
    public void init() {
        @NotNull final UserLoginResponse loginResponse = authEndpoint.login(new UserLoginRequest("test", "test"));
        token = loginResponse.getToken();
        Assert.assertThrows(Exception.class, () -> projectEndpoint.clearProject(new ProjectClearRequest(token)));
        @NotNull final ProjectCreateResponse createResponse = projectEndpoint.createProject(
                new ProjectCreateRequest(token, "test", "test")
        );
        projectBefore = createResponse.getProject();
    }

    @Test
    public void changeProjectStatusById() {
        Assert.assertThrows(Exception.class, () -> projectEndpoint.changeProjectStatusById(
                new ProjectChangeStatusByIdRequest(null, projectBefore.getId(), Status.IN_PROGRESS)));
        Assert.assertThrows(Exception.class, () -> projectEndpoint.changeProjectStatusById(
                new ProjectChangeStatusByIdRequest(token, projectBefore.getId(), null)));
        ProjectChangeStatusByIdResponse response = projectEndpoint.changeProjectStatusById(
                new ProjectChangeStatusByIdRequest(token, projectBefore.getId(), Status.IN_PROGRESS));
        Assert.assertNotNull(response);
        @Nullable Project project = response.getProject();
        Assert.assertNotEquals(projectBefore.getStatus(), project.getStatus());
    }

    @Test
    public void createProject() {
        Assert.assertThrows(Exception.class, () -> projectEndpoint.createProject(
                new ProjectCreateRequest(null, "", "")));
        Assert.assertThrows(Exception.class, () -> projectEndpoint.createProject(
                new ProjectCreateRequest(token, "", "")));
        @NotNull final String projectName = "name";
        @NotNull final ProjectCreateResponse response = projectEndpoint.createProject(
                new ProjectCreateRequest(token, "projecttakoi", "description"));
        Assert.assertNotNull(response);
        @Nullable Project project = response.getProject();
        Assert.assertEquals(projectName, project.getName());
    }

    @Test
    public void removeProjectByIdTest() {
        Assert.assertThrows(Exception.class, () -> projectEndpoint.removeProjectById(new ProjectRemoveByIdRequest(null, projectBefore.getId())));
        Assert.assertThrows(Exception.class, () -> projectEndpoint.removeProjectById(new ProjectRemoveByIdRequest(token, projectBefore.getId())));
        Assert.assertNull(projectEndpoint.listProject(new ProjectListRequest(token)).getProjects());
    }

    @Test
    public void showProjectById() {
        Assert.assertThrows(Exception.class, () -> projectEndpoint.showProjectById(new ProjectShowByIdRequest(null, projectBefore.getId())));
        @NotNull final ProjectShowByIdResponse response = projectEndpoint.showProjectById(
                new ProjectShowByIdRequest(token, projectBefore.getId()));
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getProject());
    }

    @Test
    public void updateProjectById() {
        Assert.assertThrows(Exception.class, () -> projectEndpoint.updateProjectById(
                new ProjectUpdateByIdRequest(null, projectBefore.getId(), "", "")));
        @NotNull final ProjectUpdateByIdResponse response = projectEndpoint.updateProjectById(
                new ProjectUpdateByIdRequest(token, projectBefore.getId(), "project1", "description"));
        Assert.assertNotNull(response);
        @Nullable Project project = response.getProject();
        Assert.assertNotNull(project);
        Assert.assertNotEquals(projectBefore.getName(), project.getName());
    }

}
