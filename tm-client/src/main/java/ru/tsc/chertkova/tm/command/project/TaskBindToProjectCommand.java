package ru.tsc.chertkova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.chertkova.tm.api.endpoint.ITaskEndpoint;
import ru.tsc.chertkova.tm.dto.request.task.TaskBindToProjectRequest;
import ru.tsc.chertkova.tm.util.TerminalUtil;

@Component
public final class TaskBindToProjectCommand extends AbstractProjectCommand {

    @Nullable
    @Autowired
    protected ITaskEndpoint taskEndpoint;

    @NotNull
    public static final String NAME = "bind-task-to-project";

    @NotNull
    public static final String DESCRIPTION = "Bind task to project.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        @Nullable final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        @Nullable final String taskId = TerminalUtil.nextLine();
        taskEndpoint.bindTaskToProject(new TaskBindToProjectRequest(getToken(), projectId, taskId));
    }

}
