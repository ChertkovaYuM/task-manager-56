package ru.tsc.chertkova.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.chertkova.tm.api.model.ICommand;
import ru.tsc.chertkova.tm.api.service.ICommandService;
import ru.tsc.chertkova.tm.api.service.IPropertyService;
import ru.tsc.chertkova.tm.api.service.ITokenService;
import ru.tsc.chertkova.tm.enumerated.Role;

@Component
public abstract class AbstractCommand implements ICommand {

    @NotNull
    @Autowired
    protected ITokenService tokenService;

    @Nullable
    public abstract String getName();

    @Nullable
    public abstract String getDescription();

    @Nullable
    public abstract String getArgument();

    public abstract void execute();

    @Nullable
    public abstract Role[] getRoles();

    @Nullable
    @Override
    public String toString() {
        @Nullable final String name = getName();
        @Nullable final String argument = getArgument();
        @Nullable final String description = getDescription();
        @NotNull String result = "";
        if (name != null && !name.isEmpty()) result += name + " : ";
        if (argument != null && !argument.isEmpty()) result += argument + " : ";
        if (description != null && !description.isEmpty()) result += description;
        return result;
    }

    @Nullable
    protected String getToken() {
        return tokenService.getToken();
    }

    protected void setToken(@Nullable final String token) {
        tokenService.setToken(token);
    }

}
