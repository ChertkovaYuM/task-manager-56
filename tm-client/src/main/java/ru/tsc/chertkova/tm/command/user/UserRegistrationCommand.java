package ru.tsc.chertkova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.chertkova.tm.dto.request.user.UserRegistryRequest;
import ru.tsc.chertkova.tm.enumerated.Role;
import ru.tsc.chertkova.tm.util.TerminalUtil;

@Component
public final class UserRegistrationCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "user-registry-profile";

    @NotNull
    public static final String DESCRIPTION = "Registration user profile.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[USER REGISTRATION PROFILE]");
        System.out.println("ENTER LOGIN:");
        @Nullable final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @Nullable final String password = TerminalUtil.nextLine();
        System.out.println("ENTER E-MAIL:");
        @Nullable final String email = TerminalUtil.nextLine();
        getUserEndpoint().registryUser(new UserRegistryRequest(login, password, email));
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
