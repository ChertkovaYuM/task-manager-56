package ru.tsc.chertkova.tm.command.domain;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.chertkova.tm.dto.request.data.DataBackupLoadRequest;
import ru.tsc.chertkova.tm.enumerated.Role;

@Component
public final class DomainBackupLoadCommand extends AbstractDomainCommand {

    @NotNull
    public static final String NAME = "data-backup-load";

    @NotNull
    public static final String DESCRIPTION = "Load backup from file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final DataBackupLoadRequest request = new DataBackupLoadRequest(getToken());
        getDomainEndpoint().loadDataBackup(request);
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
