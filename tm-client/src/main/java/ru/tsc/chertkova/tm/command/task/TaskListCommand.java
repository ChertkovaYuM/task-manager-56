package ru.tsc.chertkova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.chertkova.tm.dto.request.task.TaskListRequest;
import ru.tsc.chertkova.tm.enumerated.Sort;
import ru.tsc.chertkova.tm.model.Task;
import ru.tsc.chertkova.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

@Component
public final class TaskListCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-list";

    @NotNull
    public static final String DESCRIPTION = "Show task list.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST]");
        //System.out.println("[ENTER SORT:]");
        //System.out.println(Arrays.toString(Sort.values()));
        //@Nullable final String sortType = TerminalUtil.nextLine();
        //@Nullable final Sort sort = Sort.toSort(sortType);
        @Nullable final List<Task> tasks = getTaskEndpoint()
                .listTask(new TaskListRequest(getToken())).getTasks();
        renderTasks(tasks);
    }

}
