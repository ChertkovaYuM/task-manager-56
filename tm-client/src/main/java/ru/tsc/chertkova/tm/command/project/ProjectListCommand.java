package ru.tsc.chertkova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.chertkova.tm.dto.request.project.ProjectListRequest;
import ru.tsc.chertkova.tm.enumerated.Sort;
import ru.tsc.chertkova.tm.model.Project;
import ru.tsc.chertkova.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

@Component
public final class ProjectListCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-list";

    @NotNull
    public static final String DESCRIPTION = "Show project list.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        //System.out.println("[ENTER SORT:]");
        //System.out.println(Arrays.toString(Sort.values()));
        //@Nullable final String sortType = TerminalUtil.nextLine();
        //@Nullable final Sort sort = Sort.toSort(sortType);
        @Nullable final List<Project> projects = projectEndpoint.listProject(
                new ProjectListRequest(getToken())).getProjects();
        renderProjects(projects);
    }

}
